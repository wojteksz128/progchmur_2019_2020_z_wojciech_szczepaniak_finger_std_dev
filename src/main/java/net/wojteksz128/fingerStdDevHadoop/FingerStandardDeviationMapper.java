package net.wojteksz128.fingerStdDevHadoop;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.wojteksz128.fingerStdDevHadoop.model.FingerMeasureSample;
import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFinger;
import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFingerMeasureSample;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class FingerStandardDeviationMapper extends Mapper<Object, Text, Text, SeriesFingerMeasureSample> {

    private ObjectMapper objectMapper = new ObjectMapper();
    private Text text = new Text();
    private SeriesFinger seriesFinger = new SeriesFinger();
    private SeriesFingerMeasureSample fingerSample = new SeriesFingerMeasureSample();

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        FingerMeasureSample sample = objectMapper.readValue(value.getBytes(), FingerMeasureSample.class);

        // First finger
        writeFingerMeasureSample(context, sample, 1, sample.getFeatures2D().getFirst());

        // Second finger
        writeFingerMeasureSample(context, sample, 2, sample.getFeatures2D().getSecond());

        // Third finger
        writeFingerMeasureSample(context, sample, 3, sample.getFeatures2D().getThird());

        // Fourth finger
        writeFingerMeasureSample(context, sample, 4, sample.getFeatures2D().getFourth());

        // Fifth finger
        writeFingerMeasureSample(context, sample, 5, sample.getFeatures2D().getFifth());
    }

    private void writeFingerMeasureSample(Context context, FingerMeasureSample sample, int fingerNo, Double measuredValue) throws IOException, InterruptedException {
        seriesFinger.setSeries(sample.getSeries());
        seriesFinger.setSide(sample.getSide());
        seriesFinger.setFinger(fingerNo);
        fingerSample.setFinger(seriesFinger);
        fingerSample.setValue(measuredValue);
        text.set(seriesFinger.toString());
        context.write(text, fingerSample);
    }
}

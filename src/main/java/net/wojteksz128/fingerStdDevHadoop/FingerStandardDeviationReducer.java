package net.wojteksz128.fingerStdDevHadoop;

import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFingerMeasureSample;
import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFingerStatistics;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FingerStandardDeviationReducer extends Reducer<Text, SeriesFingerMeasureSample, Text, SeriesFingerStatistics> {

    private List<Double> list = new ArrayList<>();
    private Text text = new Text();
    private SeriesFingerStatistics tuple = new SeriesFingerStatistics();

    @Override
    protected void reduce(Text key, Iterable<SeriesFingerMeasureSample> values, Context context) throws IOException, InterruptedException {
        double sum = 0;
        list.clear();
        text.set(key.toString());
//        tuple.setFinger(key);
        tuple.setNumberOf(0);
        tuple.setMedian(0.0);
        tuple.setMean(0.0);
        tuple.setStandardDeviation(0.0);

        for (SeriesFingerMeasureSample sample : values) {
            sum = sum + sample.getValue();
            list.add(sample.getValue());
        }
        Collections.sort(list);
        tuple.setNumberOf(list.size());

        double median;
        if (tuple.getNumberOf() % 2 == 0) {
            double medianSum = list.get((tuple.getNumberOf() / 2) - 1) + list.get(tuple.getNumberOf() / 2);
            median = medianSum / 2;
        } else {
            median = list.get(tuple.getNumberOf() / 2);
        }
        tuple.setMedian(median);

        tuple.setMean(sum / tuple.getNumberOf());

        double sumOfSquares = 0;
        for (double value : list) {
            sumOfSquares += (value - tuple.getMean()) * (value - tuple.getMean());
        }
        tuple.setStandardDeviation(Math.sqrt(sumOfSquares / (tuple.getNumberOf() - 1)));
        context.write(text, tuple);
    }
}

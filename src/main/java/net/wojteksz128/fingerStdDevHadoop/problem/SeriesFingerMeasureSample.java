package net.wojteksz128.fingerStdDevHadoop.problem;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import static java.text.MessageFormat.format;

@Getter
@Setter
public class SeriesFingerMeasureSample implements Writable {

    private SeriesFinger finger;
    private Double value;

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        getFinger().write(dataOutput);
        dataOutput.writeDouble(getValue());
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        setFinger(new SeriesFinger());
        getFinger().readFields(dataInput);
        setValue(dataInput.readDouble());
    }

    @Override
    public String toString() {
        return format("{0} = {1}", finger, value);
    }
}

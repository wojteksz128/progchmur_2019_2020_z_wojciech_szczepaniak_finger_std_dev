package net.wojteksz128.fingerStdDevHadoop.problem;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.RawComparator;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparator;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

import static java.text.MessageFormat.format;
import static org.apache.hadoop.io.WritableUtils.readString;
import static org.apache.hadoop.io.WritableUtils.writeString;

@Getter
@Setter
public class SeriesFinger implements Serializable, Writable {

    private Integer series;
    private String side;
    private Integer finger;

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(getSeries());
        writeString(dataOutput, side);
        dataOutput.writeInt(finger);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        setSeries(dataInput.readInt());
        setSide(readString(dataInput));
        setFinger(dataInput.readInt());
    }

    @Override
    public String toString() {
        return format("Series_{0}_Side_{1}_Finger_{2}", getSeries(), getSide(), getFinger());
    }
}

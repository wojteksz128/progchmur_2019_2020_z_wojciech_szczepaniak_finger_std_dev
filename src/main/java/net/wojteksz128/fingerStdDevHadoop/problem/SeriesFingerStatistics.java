package net.wojteksz128.fingerStdDevHadoop.problem;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.hadoop.io.Writable;
import org.codehaus.jackson.annotate.JsonProperty;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;

@Getter
@Setter
@ToString
public class SeriesFingerStatistics implements Serializable, Writable {

    @JsonProperty("number-of")
    private Integer numberOf;
    @JsonProperty("median")
    private Double median;
    @JsonProperty("mean")
    private Double mean;
    @JsonProperty("standard-deviation")
    private Double standardDeviation;


    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeInt(getNumberOf());
        dataOutput.writeDouble(getMedian());
        dataOutput.writeDouble(getMean());
        dataOutput.writeDouble(getStandardDeviation());
    }

    public void readFields(DataInput dataInput) throws IOException {
        setNumberOf(dataInput.readInt());
        setMedian(dataInput.readDouble());
        setMean(dataInput.readDouble());
        setStandardDeviation(dataInput.readDouble());
    }
}

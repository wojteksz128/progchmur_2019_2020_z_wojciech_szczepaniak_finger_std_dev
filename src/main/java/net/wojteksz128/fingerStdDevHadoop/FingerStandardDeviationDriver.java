package net.wojteksz128.fingerStdDevHadoop;

import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFingerMeasureSample;
import net.wojteksz128.fingerStdDevHadoop.problem.SeriesFingerStatistics;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;

public class FingerStandardDeviationDriver {

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.err.println("Not enough parameters specified. Add <input file path> at first parameter and <output dir path> at second.");
        }

        /* set the hadoop system parameter */

        System.setProperty("hadoop.home.dir", "/usr/local/hadoop");

        Configuration conf = ConfigurationFactory.getInstance();
        Job job = Job.getInstance(conf);
        job.setJarByClass(FingerStandardDeviationDriver.class);
        job.setJobName("Calculate_Standard_Deviation_For_Finger_Measure_Data");
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        job.setMapperClass(FingerStandardDeviationMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(SeriesFingerMeasureSample.class);
        job.setReducerClass(FingerStandardDeviationReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(SeriesFingerStatistics.class);
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

package net.wojteksz128.fingerStdDevHadoop;

import org.apache.hadoop.conf.Configuration;

public class ConfigurationFactory {

    public static Configuration getInstance() {
        return new Configuration();
    }
}

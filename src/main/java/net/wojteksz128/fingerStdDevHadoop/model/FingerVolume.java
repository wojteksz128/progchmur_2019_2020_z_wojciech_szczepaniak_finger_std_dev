
package net.wojteksz128.fingerStdDevHadoop.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "little",
        "index",
        "thumb",
        "middle",
        "ring"
})
@Getter
@Setter
@ToString
public class FingerVolume implements Serializable {

    @JsonProperty("little")
    private Double little;
    @JsonProperty("index")
    private Double index;
    @JsonProperty("thumb")
    private Double thumb;
    @JsonProperty("middle")
    private Double middle;
    @JsonProperty("ring")
    private Double ring;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

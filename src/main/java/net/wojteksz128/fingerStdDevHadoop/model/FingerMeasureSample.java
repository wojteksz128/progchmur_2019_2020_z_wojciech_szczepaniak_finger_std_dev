
package net.wojteksz128.fingerStdDevHadoop.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static org.apache.hadoop.io.WritableUtils.readString;
import static org.apache.hadoop.io.WritableUtils.writeString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timestamp",
        "features2D",
        "side",
        "features3D",
        "series",
        "sample"
})
@Getter
@Setter
@ToString
public class FingerMeasureSample implements Serializable {

    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("features2D")
    private Features2D features2D;
    @JsonProperty("side")
    private String side;
    @JsonProperty("features3D")
    private Features3D features3D;
    @JsonProperty("series")
    private Integer series;
    @JsonProperty("sample")
    private Integer sample;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}

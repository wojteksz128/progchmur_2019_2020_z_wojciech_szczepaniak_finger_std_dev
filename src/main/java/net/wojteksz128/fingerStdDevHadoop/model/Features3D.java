
package net.wojteksz128.fingerStdDevHadoop.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static org.apache.hadoop.io.WritableUtils.readString;
import static org.apache.hadoop.io.WritableUtils.writeString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "palm-section",
        "defects-distance-proportion",
        "finger-length",
        "no-finger-shape-factor",
        "finger-volume"
})
@Getter
@Setter
@ToString
public class Features3D implements Serializable {

    @JsonProperty("palm-section")
    private PalmSection palmSection;
    @JsonProperty("defects-distance-proportion")
    private String defectsDistanceProportion;
    @JsonProperty("finger-length")
    private String fingerLength;
    @JsonProperty("no-finger-shape-factor")
    private NoFingerShapeFactor noFingerShapeFactor;
    @JsonProperty("finger-volume")
    private FingerVolume fingerVolume;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
